# Virtdeskbar

virtdeskbar is virtual desktop pager similar found in many linux desktop enviroments.

It is based on MScholtes VirtualDesktop (<https://github.com/MScholtes/VirtualDesktop>)

![Example](./docs/example.png)

## Versions

|         | Windows Version                |
| ------- | ------------------------------ |
| 11-24H1 | Win 11 3085 and up, 2024/02/15 |

## Usage

```powershell
.\virtdeskbar.exe --config
.\virtdeskbar.exe --help
.\virtdeskbar.exe --verbose
.\virtdeskbar.exe --version
```

- To "install" virtdeskbar copy `virtdeskbar.exe`, `virtdeskbar.ini` and `CommandLine.dll` into a directory
- If `--config` is not set, `virtdeskbar.exe` looks in current directory for a `virtdeskbar.ini`
- Icons should be PNGs with transparent background. Best 512x512 pixels

## Develop & Build

### General

#### Install libs

```powershell
nuget.exe install ini-parser -Version 2.5.2 -OutputDirectory packages
```

Add manually to `packages.config`

```xml
<package id="ini-parser" version="2.5.2" />
```

### Topbar

In Powershell:

```powershell
cd topbar

# Compile topbar.exe
.\make.bat
```

### Virtdeskbar 11 24H1

In Powershell:

```powershell
cd virtdeskbar-11-24H1
# Install packages
nuget.exe Install packages.config -OutputDirectory packages
cp packages\CommandLineParser.2.8.0\lib\net40\CommandLine.dll .

# Compile virtdeskbar.exe
.\make.bat

# Update virtdeskbar.ini and then start with --verbose set to see errors
.\virtdeskbar.exe --verbose
```

### Virtdeskbar 11 23H2

In Powershell:

```powershell
cd virtdeskbar-11-23H2
# Install packages
nuget.exe Install packages.config -OutputDirectory packages
cp packages\CommandLineParser.2.8.0\lib\net40\CommandLine.dll .

# Compile virtdeskbar.exe
.\make.bat

# Update virtdeskbar.ini and then start with --verbose set to see errors
.\virtdeskbar.exe --verbose
```

### Virtdeskbar 11 22H2

In Powershell:

```powershell
cd virtdeskbar-11-22H2
# Install packages
nuget.exe Install packages.config -OutputDirectory packages
cp packages\CommandLineParser.2.8.0\lib\net40\CommandLine.dll .

# Compile virtdeskbar.exe
.\make.bat

# Update virtdeskbar.ini and then start with --verbose set to see errors
.\virtdeskbar.exe --verbose
```

### Virtdeskbar 11

In Powershell:

```powershell
cd virtdeskbar-11
# Install packages
nuget.exe Install packages.config -OutputDirectory packages
cp packages\CommandLineParser.2.8.0\lib\net40\CommandLine.dll .

# Compile virtdeskbar.exe
.\make.bat

# Update virtdeskbar.ini and then start with --verbose set to see errors
.\virtdeskbar.exe --verbose
```
