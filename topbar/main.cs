


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;



using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;

using CommandLine;
using CommandLine.Text;

namespace topbar
{


	

		/// <span class="code-SummaryComment"><summary></span>
		/// Summary description for Form1.
		/// <span class="code-SummaryComment"></summary></span>
		public class MainForm : System.Windows.Forms.Form
		{
				/// <span class="code-SummaryComment"><summary></span>
				/// Required designer variable.
				/// <span class="code-SummaryComment"></summary></span>
				private System.ComponentModel.Container components = null;


				private static int topHeight = 20;


				private static bool verbose;
				private static Color backgroundColor;
				private static Color textColor;
				private static string fontFamiliy = "MS Sans";
				private static int fontSize;

				private static Color batteryOutlineColor;
				private static Color batteryBackgroundColor; //Should be backgroundColor
				private static Color batteryChargedColor;
				private static Color batteryUnchargedColor;
				private static int batteryWidth;
				private static int batteryHeight;


				private static Color closebuttonBorderColor;
				private static Color closebuttonBackgroundColorMouseDown;
				private static Color closebuttonBackgroundColorMouseHover;

				public MainForm(bool verb, string configFile)
				{
					
					verbose = verb;

					Config themeConfig = new Config(configFile,"theme",verbose);

					backgroundColor = themeConfig.GetColor("backgroundColor", Color.FromArgb(255, 238, 238, 238));
					textColor = themeConfig.GetColor("textColor", Color.FromArgb(255, 90, 90, 90));
					fontFamiliy = themeConfig.GetString("fontFamiliy", "MS Sans");
					fontSize = themeConfig.GetInt("fontSize", 10);



					batteryOutlineColor = themeConfig.GetColor("batteryOutlineColor", Color.FromArgb(255, 60, 60, 60));
					batteryChargedColor = themeConfig.GetColor("batteryChargedColor", Color.FromArgb(255, 70, 70, 70));
					batteryUnchargedColor = themeConfig.GetColor("batteryUnchargedColor", Color.FromArgb(255, 140, 150, 140));
					batteryBackgroundColor = backgroundColor;
					batteryWidth = themeConfig.GetInt("batteryWidth", 30);
					batteryHeight = themeConfig.GetInt("batteryHeight", 14);


					closebuttonBorderColor = themeConfig.GetColor("closebuttonBorderColor", Color.FromArgb(255, 200, 200, 200));
					closebuttonBackgroundColorMouseDown = themeConfig.GetColor("closebuttonBackgroundColorMouseDown", Color.FromArgb(255, 240, 100, 100));
					closebuttonBackgroundColorMouseHover = themeConfig.GetColor("closebuttonBackgroundColorMouseHover", Color.FromArgb(255, 220, 50, 50));


					InitializeComponent();
					
					//SetProcessDPIAwareness();
					
					var t = Task.Run(() =>
					{
						if (verbose) Console.WriteLine("Starting background task");
						if (verbose) Console.WriteLine("Task thread ID: {0}", Thread.CurrentThread.ManagedThreadId);

						//Wait a second before getting the process id to pin this window to all desktops
						//Thread.Sleep(1000);
						Task.Delay(1000).Wait();
						Process oCurrent = Process.GetCurrentProcess();
						System.IntPtr iParam = oCurrent.MainWindowHandle;
						if (verbose) Console.WriteLine("MainWindowHandle: {0}", iParam.ToString());
						//VirtualDesktop11.Desktop.PinWindow(iParam);
						if (verbose) Console.WriteLine("Starting background loop");
						while (true) 
						{

							//hightlight active desktop button if change of virtual desktop was done outside this tool
							//HighlightActiveDesktopButton();

							int millisecondsToWait = 1000;

							Stopwatch stopwatch = Stopwatch.StartNew();
							while (true)
							{
								//some other processing to do possible
								if (stopwatch.ElapsedMilliseconds >= millisecondsToWait)
								{
									break;
								}
								Task.Delay(1).Wait();
								//Thread.Sleep(1);
								
							}
							clockTb.Text = DateTime.Now.ToString("dd\\.MM\\.yyyy HH\\:mm");
							if (verbose) Console.WriteLine("Get datetime: {0}",DateTime.Now.ToString("dd\\.MM\\.yyyy HH\\:mm"));

							PowerStatus status = SystemInformation.PowerStatus;

							bool powerPluggedIn = false;

							if (status.PowerLineStatus == PowerLineStatus.Online)
							{
								powerPluggedIn = true;
								if (verbose) Console.WriteLine("Power plugged in");
							} else {
								powerPluggedIn = false;
								if (verbose) Console.WriteLine("Power NOT plugged in");
							}


							battery = draw.DrawBattery(status.BatteryLifePercent,
								powerPluggedIn,
								batteryWidth,
								batteryHeight,
								batteryBackgroundColor,
								batteryOutlineColor,
								batteryChargedColor,
								batteryUnchargedColor,
								false);


							batteryIcon.Image = battery;
							batteryIcon.BringToFront();
							batteryIcon.Refresh();
							batteryIcon.Visible = true;

							float batteryPercent = status.BatteryLifePercent;
							string batteryString = batteryPercent.ToString("P0");
							batteryTb.Text = /*"B: " +*/ batteryString;

							// if (verbose) Console.WriteLine("Get battery: {0}",batteryString);
							// if (verbose) Console.WriteLine("Get battery: {0}",batteryPercent);


							if (status.PowerLineStatus == PowerLineStatus.Online)
							{
								if (batteryPercent < 1.0f) {
									if (verbose) Console.WriteLine("Charging");
								} else {
									if (verbose) Console.WriteLine("Fully charged");
								}
							} 

							//string percent_text = percent.ToString("P0");

							// if (status.PowerLineStatus == PowerLineStatus.Online)
							// {
							// 		if (percent < 1.0f)
							// 				batteryTb.Text = percent_text + ", charging";
							// 		else
							// 				batteryTb.Text = "Online fully charged";
							// }
							// else
							// {
							// 		batteryTb.Text = "Offline, " + percent_text + " remaining";
							// }

							//Thread.Sleep(500);

							float volume = VideoPlayerController.AudioManager.GetMasterVolume();
							string volumeString = volume.ToString("00");
							volumeTb.Text = "V: " + volumeString + " %";

						}
						

					} );


					//
					// TODO: Add any constructor code after InitializeComponent call
					//
				}

				/// <span class="code-SummaryComment"><summary></span>
				/// Clean up any resources being used.
				/// <span class="code-SummaryComment"></summary></span>
				protected override void Dispose( bool disposing )
				{
					if( disposing )
					{
						if (components != null) 
						{
							components.Dispose();
						}
					}
					base.Dispose( disposing );
				}

				#region Windows Form Designer generated code
				/// <span class="code-SummaryComment"><summary></span>
				/// Required method for Designer support - do not modify
				/// the contents of this method with the code editor.
				/// <span class="code-SummaryComment"></summary></span>

				private static Label clockTb;
				private static Label volumeTb;
				private static Label batteryTb;
				private static Bitmap battery;
				private static PictureBox batteryIcon;
				private void InitializeComponent()
				{

					// 
					// MainForm
					// 
					this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
					this.ClientSize = new System.Drawing.Size(960, 5);
					this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
					this.Name = "Topbar";
					this.Text = "Topbar";
					this.BackColor = backgroundColor;
					this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
					this.Load += new System.EventHandler(this.OnLoad);





					TableLayoutPanel mainLayout = new TableLayoutPanel();
					mainLayout.RowCount = 1;
					mainLayout.Dock = DockStyle.Fill;
					mainLayout.BackColor = Color.FromArgb(0, 0, 0, 0); //Transparent
					mainLayout.BorderStyle = BorderStyle.None;

					mainLayout.ColumnStyles.Clear();
					//menu part on left side in with an absolute width
					//main area: Use 100% of the rest space
					//mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent,100));





					mainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100));





					TableLayoutPanel watchPanel = new TableLayoutPanel();
					watchPanel.Dock = DockStyle.Fill;
					watchPanel.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					watchPanel.BorderStyle = BorderStyle.None;
					watchPanel.BorderStyle = BorderStyle.None;
					watchPanel.Margin = new Padding(0, 0, 0, 0);

					clockTb = new Label();
					clockTb.Dock = DockStyle.Fill;
					clockTb.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					clockTb.ForeColor = textColor; //Color.FromArgb(255,180,180,180); //Color.FromArgb(255, 0, 0, 0);
					clockTb.Font = new Font(fontFamiliy, fontSize, FontStyle.Bold);
					clockTb.BorderStyle = BorderStyle.None;
					clockTb.Margin = new Padding(2, 2, 2, 2);
					clockTb.TextAlign = ContentAlignment.TopLeft;
					clockTb.Text = DateTime.Now.ToString("dd\\.MM\\.yyyy HH\\:mm");
					watchPanel.Controls.Add(clockTb);




					TableLayoutPanel batteryPanel = new TableLayoutPanel();
					batteryPanel.Dock = DockStyle.Fill;
					batteryPanel.BackColor = backgroundColor; //Color.FromArgb(255, 0, 0, 0);
					batteryPanel.BorderStyle = BorderStyle.None;
					batteryPanel.BorderStyle = BorderStyle.None;
					batteryPanel.Margin = new Padding(0, 0, 0, 0);

// float percent, 
// int wid, 
// int hgt,
// Color bg_color, 
// Color outline_color,
// Color charged_color, 
// Color uncharged_color,
// bool striped

					PowerStatus status = SystemInformation.PowerStatus;


					battery = draw.DrawBattery(status.BatteryLifePercent, false,
						batteryWidth,
						batteryHeight,
						batteryBackgroundColor,
						batteryOutlineColor,
						batteryChargedColor,
						batteryUnchargedColor,
						false);
					batteryIcon = new PictureBox();
					batteryIcon.Size = new Size(40, 28);
					batteryIcon.Image = battery;
					batteryIcon.BringToFront();

					batteryTb = new Label();
					batteryTb.Dock = DockStyle.Fill;
					batteryTb.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					batteryTb.ForeColor = textColor; //Color.FromArgb(255,180,180,180); //Color.FromArgb(255, 0, 0, 0);
					batteryTb.Font = new Font(fontFamiliy, fontSize, FontStyle.Bold);
					batteryTb.BorderStyle = BorderStyle.None;
					batteryTb.Margin = new Padding(2, 2, 2, 2);
					batteryTb.TextAlign = ContentAlignment.TopLeft;
					float batteryPercent = status.BatteryLifePercent;
					string batteryString = batteryPercent.ToString("P0");
					batteryTb.Text = /*"B: " +*/ batteryString;


					batteryPanel.ColumnCount = 2;
					batteryPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 38));
					batteryPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 62));
					batteryPanel.Controls.Add(batteryIcon);
					batteryPanel.Controls.Add(batteryTb);



					TableLayoutPanel volumePanel = new TableLayoutPanel();
					volumePanel.Dock = DockStyle.Fill;
					volumePanel.BackColor = Color.FromArgb(0,30,30,30); //Color.FromArgb(255, 0, 0, 0);
					volumePanel.BorderStyle = BorderStyle.None;
					volumePanel.BorderStyle = BorderStyle.None;
					volumePanel.Margin = new Padding(0, 0, 0, 0);
					volumePanel.MouseWheel += new MouseEventHandler(VolumeOnMouseScroll);
					volumePanel.MouseMove += new MouseEventHandler(VolumeOnMouseScroll);


					volumeTb = new Label();
					volumeTb.Dock = DockStyle.Fill;
					volumeTb.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					volumeTb.ForeColor = textColor; //Color.FromArgb(255,180,180,180); //Color.FromArgb(255, 0, 0, 0);
					volumeTb.Font = new Font(fontFamiliy, fontSize, FontStyle.Bold);
					volumeTb.BorderStyle = BorderStyle.None;
					volumeTb.Margin = new Padding(2, 2, 2, 2);
					volumeTb.TextAlign = ContentAlignment.TopLeft;
					float volume = VideoPlayerController.AudioManager.GetMasterVolume();
					string volumeString = volume.ToString("00");
					volumeTb.Text = "V: " + volumeString + " %";
					volumePanel.Controls.Add(volumeTb);


					TableLayoutPanel menuPanel = new TableLayoutPanel();

					menuPanel.ColumnCount = 1;
					menuPanel.RowCount = 2;
					menuPanel.Dock = DockStyle.Fill;
					menuPanel.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					menuPanel.BorderStyle = BorderStyle.None;
					menuPanel.Margin = new Padding(0, 0, 0, 0);


					menuPanel.RowStyles.Clear();
					menuPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20));
					menuPanel.RowStyles.Add(new RowStyle(SizeType.Percent,100));
					menuPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
					menuPanel.AutoSize = true;


					Button closeButton = new Button();
					closeButton.Text = "";
					closeButton.Visible = true;
					closeButton.Dock = DockStyle.Fill;
					closeButton.TabStop = false;
					// button.FlatAppearance.BorderSize = 0;
					closeButton.Margin = new Padding(2, 2, 2, 2);
					closeButton.BackColor = backgroundColor;
					closeButton.ForeColor = Color.FromArgb(255, 50, 0, 50); //Textcolor

					closeButton.FlatStyle = FlatStyle.Flat;
					closeButton.FlatAppearance.BorderColor = closebuttonBorderColor; 
					closeButton.FlatAppearance.BorderSize = 1;
					closeButton.FlatAppearance.MouseDownBackColor = closebuttonBackgroundColorMouseDown;
					closeButton.FlatAppearance.MouseOverBackColor = closebuttonBackgroundColorMouseHover;
					closeButton.Click += new System.EventHandler(CloseButtonClick);
					//moveButton.Cursor = System.Windows.Forms.Cursors.SizeAll; //SizeAll = Move window Cursor

					menuPanel.Controls.Add(closeButton);



					TableLayoutPanel spacer = new TableLayoutPanel();
					spacer.ColumnCount = 1;
					spacer.RowCount = 1;
					spacer.Dock = DockStyle.Fill;
					spacer.BackColor = Color.FromArgb(0,0,0,0); //Color.FromArgb(255, 0, 0, 0);
					spacer.BorderStyle = BorderStyle.None;
					spacer.Margin = new Padding(0, 0, 0, 0);
					spacer.AutoSize = true;
					spacer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));



					


					mainLayout.ColumnCount = 5;
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100)); //Spacer
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 110)); //Battery
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute,65)); //Volume
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 130)); //clock
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20)); //Close
					mainLayout.Controls.Add(spacer);
					mainLayout.Controls.Add(batteryPanel);
					mainLayout.Controls.Add(volumePanel);
					mainLayout.Controls.Add(watchPanel);
					mainLayout.Controls.Add(menuPanel);

					

					this.Controls.Add(mainLayout);
				}


				public static Image resizeImage(Image imgToResize, Size size)
				{
						return (Image)(new Bitmap(imgToResize, size));
				}

				static void CloseButtonClick(object sender, EventArgs e) 
				{
					System.Windows.Forms.Application.Exit();
				}

				static void VolumeOnMouseScroll(object sender, MouseEventArgs  e) 
				{

					if (Convert.ToString(e.Delta) == "120") {
						Console.WriteLine("Volume UP");
						keybd_event((byte)Keys.VolumeUp, 0, 0, 0);
					} else if (Convert.ToString(e.Delta) == "-120") {
						Console.WriteLine("Volume DOWN");
						keybd_event((byte)Keys.VolumeDown, 0, 0, 0);
					}
					float volume = VideoPlayerController.AudioManager.GetMasterVolume();
					string volumeString = volume.ToString("00");
					volumeTb.Text = "V: " + volumeString + " %";
				}

				// static int PlayerVolume()
				// {
				// 		RecordPlayer rp = new RecordPlayer();
				// 		rp.PlayerID = -1;
				// 		int playerVolume = rp.PlayerVolume;
				// 		return playerVolume;
				// }


				#endregion




				#region APPBAR

				[StructLayout(LayoutKind.Sequential)]
				struct RECT
				{
					public int left;
					public int top;
					public int right;
					public int bottom;
				}

				[StructLayout(LayoutKind.Sequential)]
				struct APPBARDATA
				{
					public int cbSize;
					public IntPtr hWnd;
					public int uCallbackMessage;
					public int uEdge;
					public RECT rc;
					public IntPtr lParam;
				}

				enum ABMsg : int
				{
					ABM_NEW=0,
					ABM_REMOVE=1,
					ABM_QUERYPOS=2,
					ABM_SETPOS=3,
					ABM_GETSTATE=4,
					ABM_GETTASKBARPOS=5,
					ABM_ACTIVATE=6,
					ABM_GETAUTOHIDEBAR=7,
					ABM_SETAUTOHIDEBAR=8,
					ABM_WINDOWPOSCHANGED=9,
					ABM_SETSTATE=10
				}

				enum ABNotify : int
				{
					ABN_STATECHANGE=0,
					ABN_POSCHANGED,
					ABN_FULLSCREENAPP,
					ABN_WINDOWARRANGE
				}

				enum ABEdge : int
				{
					ABE_LEFT=0,
					ABE_TOP,
					ABE_RIGHT,
					ABE_BOTTOM
				}

				private bool fBarRegistered = false;

				[DllImport("user32.dll")]
				static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, int dwExtraInfo);
				[DllImport("SHELL32", CallingConvention=CallingConvention.StdCall)]
				static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);
				[DllImport("USER32")]
				static extern int GetSystemMetrics(int Index);
				[DllImport("User32.dll", ExactSpelling=true, 
					CharSet=System.Runtime.InteropServices.CharSet.Auto)]
				private static extern bool MoveWindow
					(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);
				[DllImport("User32.dll", CharSet=CharSet.Auto)]
				private static extern int RegisterWindowMessage(string msg);
				private int uCallBack;

				private void RegisterBar()
				{
					APPBARDATA abd = new APPBARDATA();
					abd.cbSize = Marshal.SizeOf(abd);
					abd.hWnd = this.Handle;
					if (!fBarRegistered)
					{
						uCallBack = RegisterWindowMessage("AppBarMessage");
						abd.uCallbackMessage = uCallBack;

						uint ret = SHAppBarMessage((int)ABMsg.ABM_NEW, ref abd);
						fBarRegistered = true;

						ABSetPos();
					}
					else
					{
						SHAppBarMessage((int)ABMsg.ABM_REMOVE, ref abd);
						fBarRegistered = false;
					}
				}

				private void ABSetPos()
				{
					APPBARDATA abd = new APPBARDATA();
					abd.cbSize = Marshal.SizeOf(abd);
					abd.hWnd = this.Handle;
					abd.uEdge = (int)ABEdge.ABE_TOP;

					if (abd.uEdge == (int)ABEdge.ABE_LEFT || abd.uEdge == (int)ABEdge.ABE_RIGHT) 
					{
						abd.rc.top = 0;
						abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
						if (abd.uEdge == (int)ABEdge.ABE_LEFT) 
						{
							abd.rc.left = 0;
							abd.rc.right = Size.Width;
						}
						else 
						{
							abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
							abd.rc.left = abd.rc.right - Size.Width;
						}

					}
					else 
					{
						abd.rc.left = 0;
						abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
						if (abd.uEdge == (int)ABEdge.ABE_TOP) 
						{
							abd.rc.top = 0;
							abd.rc.bottom = Size.Height;
						}
						else 
						{
							abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
							abd.rc.top = abd.rc.bottom - Size.Height;
						}
					}

					// Query the system for an approved size and position. 
					SHAppBarMessage((int)ABMsg.ABM_QUERYPOS, ref abd); 

					// Adjust the rectangle, depending on the edge to which the 
					// appbar is anchored. 
					switch (abd.uEdge) 
					{ 
						case (int)ABEdge.ABE_LEFT: 
							abd.rc.right = abd.rc.left + Size.Width;
							break; 
						case (int)ABEdge.ABE_RIGHT: 
							abd.rc.left= abd.rc.right - Size.Width;
							break; 
						case (int)ABEdge.ABE_TOP: 
							abd.rc.bottom = abd.rc.top + topHeight;//Size.Height;
							break; 
						case (int)ABEdge.ABE_BOTTOM: 
							abd.rc.top = abd.rc.bottom - topHeight; //Size.Height;
							break; 
					}

					// Pass the final bounding rectangle to the system. 
					SHAppBarMessage((int)ABMsg.ABM_SETPOS, ref abd); 

					// Move and size the appbar so that it conforms to the 
					// bounding rectangle passed to the system. 
					MoveWindow(abd.hWnd, abd.rc.left, abd.rc.top, 
						abd.rc.right - abd.rc.left, abd.rc.bottom - abd.rc.top, true); 
				}

				protected override void WndProc(ref System.Windows.Forms.Message m)
				{
					if (m.Msg == uCallBack)
					{
						switch(m.WParam.ToInt32())
						{
							case (int)ABNotify.ABN_POSCHANGED:
								ABSetPos();
								break;
					}
					}

					base.WndProc(ref m);
				}

				protected override System.Windows.Forms.CreateParams CreateParams
				{
					get
					{
						CreateParams cp = base.CreateParams;
						cp.Style &= (~0x00C00000); // WS_CAPTION
						cp.Style &= (~0x00800000); // WS_BORDER
						cp.ExStyle = 0x00000080 | 0x00000008; // WS_EX_TOOLWINDOW | WS_EX_TOPMOST
						return cp;
					}
				}

				#endregion


				public class Options
				{
					[Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
					public bool Verbose { get; set; }

					[Option('c', "config", Required = false, HelpText = "Config file")]
					public string ConfigFile { get; set; }

				}


				/// <span class="code-SummaryComment"><summary></span>
				/// The main entry point for the application.
				/// <span class="code-SummaryComment"></summary></span>

				[DllImport( "kernel32.dll" )]
				static extern bool AttachConsole( int dwProcessId );
				private const int ATTACH_PARENT_PROCESS = -1;

				[STAThread]
				static void Main(string[] args) 
				{
					bool verb = false;
					string configfile = "topbar.ini";

					var parser = new CommandLine.Parser(with => with.HelpWriter = null);

					var parserResult = parser.ParseArguments<Options>(args);
					parserResult.WithParsed<Options>(o =>
						{
							if (o.Verbose)
							{
								verb = true;
							}

							if (o.ConfigFile != "")
							{
								configfile = o.ConfigFile;
							}


						}).WithNotParsed(errs => DisplayHelp(parserResult, errs));



					if (verb == true) {
						AttachConsole( ATTACH_PARENT_PROCESS );
						Console.WriteLine("");
						Console.WriteLine("Console attached");
					}

					if (verb) {
						Console.WriteLine("Using config file: " + configfile);
					}



						Application.Run(new MainForm(verb,configfile));
				}


				static void DisplayHelp<T>(ParserResult<T> result, IEnumerable<Error> errs)
				{

					AttachConsole( ATTACH_PARENT_PROCESS );
					Console.WriteLine("");
					Console.WriteLine("");
					Console.WriteLine("==========================================");
					Console.WriteLine("=               Topbar                   =");
					Console.WriteLine("==========================================");
					Console.WriteLine("");
					Console.WriteLine("Battery:");
					Console.WriteLine("  green:  Power plugged in - 100% charged");
					Console.WriteLine("  purple: Power plugged in - still charging");
					Console.WriteLine("  red:    On battery - discharging");
					Console.WriteLine("");
					Console.WriteLine("Volume:");
					Console.WriteLine("  Hover mouse over volume and use scroll wheel to increase / decrease volume");
					Console.WriteLine("");
					Console.WriteLine("");

					HelpText helpText = null;
					if (errs.IsVersion())  //check if error is version request
						helpText = HelpText.AutoBuild(result);
					else
					{
						helpText = HelpText.AutoBuild(result, h =>
						{
							//configure help
							h.AdditionalNewLineAfterOption = false;
							h.Heading = "Topbar"; //change header
							h.Copyright = "Copyright (c) 2022 Korbinian Pauli"; //change copyright text
							return HelpText.DefaultParsingErrorsHandler(result, h);
						}, e => e);
					}
					Console.WriteLine(helpText);
					System.Environment.Exit(0);
				}


				private void OnLoad(object sender, System.EventArgs e)
				{
						RegisterBar();



				}

				private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
				{
						RegisterBar();
				}
		}
}
