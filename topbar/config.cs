using System;
using System.Drawing;

namespace topbar {

  public class Config {

    private string Section;
    private ColorConverter converter;
    private IniFile ConfigIni;
    private bool Verbose;

    public Config(string configFile, string sectionIni, bool verboseIni) {
      ConfigIni = new IniFile(configFile);
      converter = new ColorConverter();
      Section = sectionIni;
      Verbose = verboseIni;
    }

    public Color GetColor(string Key, Color stdColor) {
      var colorString = ConfigIni.Read(Key,Section);
      if (colorString == "") {
        if (Verbose) Console.WriteLine(Key + " not defined in ini file. Using standard color");
        return stdColor;
      }

      if (Verbose) Console.WriteLine(Key + "=" + colorString);
      Color color = (Color)converter.ConvertFromString(colorString);

      return color;      
    }

    public string GetString(string Key, string stdString) {
      var keyString = ConfigIni.Read(Key,Section);
      if (keyString == "") {
        if (Verbose) Console.WriteLine(Key + " not defined in ini file. Using standard string");
        return stdString;
      }
      if (Verbose) Console.WriteLine(Key + "=" + keyString);
      return keyString; 
    }

    public int GetInt(string Key, int stdInt) {
      var keyString = ConfigIni.Read(Key,Section);
      if (keyString == "") {
        if (Verbose) Console.WriteLine(Key + " not defined in ini file. Using standard integer");
        return stdInt;
      }
      if (Verbose) Console.WriteLine(Key + "=" + keyString);

      int keyInt = Int16.Parse(keyString);
      return keyInt; 
    }

  }

}