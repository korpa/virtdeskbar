using System;
using System.Runtime.InteropServices;

using System.Drawing;

namespace virtdeskbar {
  public class ScreenHelpers {
      [DllImport("gdi32.dll")]
      static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

      public enum DeviceCap
      {
          VERTRES = 10,
          DESKTOPVERTRES = 117
      } 

      public static float scaling()
      {
          Graphics g = Graphics.FromHwnd(IntPtr.Zero);
          IntPtr desktop = g.GetHdc();
          int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
          int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);

          return (float)PhysicalScreenHeight / (float)LogicalScreenHeight;
      }

      public static int logicalScreenHeight()
      {
          Graphics g = Graphics.FromHwnd(IntPtr.Zero);
          IntPtr desktop = g.GetHdc();
          int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);

          return LogicalScreenHeight;
      }

      public static int physicalScreenHeight()
      {
          Graphics g = Graphics.FromHwnd(IntPtr.Zero);
          IntPtr desktop = g.GetHdc();
          int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);

          return PhysicalScreenHeight;
      }

  }
}