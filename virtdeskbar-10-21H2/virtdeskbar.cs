using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;



using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

using System.Collections.Generic;
using System.Linq;

using CommandLine;
using CommandLine.Text;

namespace virtdeskbar
{


	

		/// <span class="code-SummaryComment"><summary></span>
		/// Summary description for Form1.
		/// <span class="code-SummaryComment"></summary></span>
		public class MainForm : System.Windows.Forms.Form
		{
				/// <span class="code-SummaryComment"><summary></span>
				/// Required designer variable.
				/// <span class="code-SummaryComment"></summary></span>
				private System.ComponentModel.Container components = null;

				private static bool verbose;
				private static Color backgroundColor = Color.FromArgb(255, 40, 0, 35);
				private static Color buttonMouseDownBackgroundColor = Color.FromArgb(255, 56, 0, 51);
				private static Color buttonMouseHoverBackgroundColor = Color.FromArgb(255, 56, 0, 51);

				
				

				//https://www.colorhexa.com/7a0c5f
				private static Color virtdeskMouseHoverBackgroundColor = Color.FromArgb(255, 122, 12, 95);
				//https://www.colorhexa.com/a91083
				private static Color virtdeskMouseHoverBorderColor = Color.FromArgb(255, 169, 16, 131);

				//https://www.colorhexa.com/1083a9
				private static Color virtdeskActiveBackgroundColor = Color.FromArgb(255, 16, 131, 169);

				private static int maxIconButtons = 20;
				private static int virtdeskbarHeight = 45;

				private IniFile ConfigIni;

				public MainForm(bool verb, string configFile)
				{
					
					verbose = verb;


					ConfigIni = new IniFile(configFile);

					ColorConverter converter = new ColorConverter();

					if (verbose) Console.WriteLine("[theme]");

					var backgroundColorString = ConfigIni.Read("backgroundColor","theme");
					if (verbose) Console.WriteLine("backgroundColor=" + backgroundColorString);
					backgroundColor = (Color)converter.ConvertFromString(backgroundColorString);

					var buttonMouseDownBackgroundColorString = ConfigIni.Read("buttonMouseDownBackgroundColor","theme");
					if (verbose) Console.WriteLine("buttonMouseDownBackgroundColor=" + buttonMouseDownBackgroundColorString);
					buttonMouseDownBackgroundColor = (Color)converter.ConvertFromString(buttonMouseDownBackgroundColorString);

					var buttonMouseHoverBackgroundColorString = ConfigIni.Read("buttonMouseHoverBackgroundColor","theme");
					if (verbose) Console.WriteLine("buttonMouseHoverBackgroundColor=" + buttonMouseHoverBackgroundColorString);
					buttonMouseHoverBackgroundColor = (Color)converter.ConvertFromString(buttonMouseHoverBackgroundColorString);

					var virtdeskMouseHoverBackgroundColorString = ConfigIni.Read("virtdeskMouseHoverBackgroundColor","theme");
					if (verbose) Console.WriteLine("virtdeskMouseHoverBackgroundColor=" + virtdeskMouseHoverBackgroundColorString);
					if (virtdeskMouseHoverBackgroundColorString != "")
					{
						virtdeskMouseHoverBackgroundColor = (Color)converter.ConvertFromString(virtdeskMouseHoverBackgroundColorString);
					}

					var virtdeskMouseHoverBorderColorString = ConfigIni.Read("virtdeskMouseHoverBorderColor","theme");
					if (verbose) Console.WriteLine("virtdeskMouseHoverBorderColor=" + virtdeskMouseHoverBorderColorString);
					if (virtdeskMouseHoverBorderColorString != "")
					{
						virtdeskMouseHoverBorderColor = (Color)converter.ConvertFromString(virtdeskMouseHoverBorderColorString);
					}

					var virtdeskActiveBackgroundColorString = ConfigIni.Read("virtdeskActiveBackgroundColor","theme");
					if (verbose) Console.WriteLine("virtdeskActiveBackgroundColor=" + virtdeskActiveBackgroundColorString);
					if (virtdeskActiveBackgroundColorString != "")
					{
						virtdeskActiveBackgroundColor = (Color)converter.ConvertFromString(virtdeskActiveBackgroundColorString);
					}

					if (verbose) Console.WriteLine("");

					InitializeComponent();

					var t = Task.Run(() =>
					{
						if (verbose) Console.WriteLine("Starting background task");
						if (verbose) Console.WriteLine("Task thread ID: {0}", Thread.CurrentThread.ManagedThreadId);

						//Wait a second before getting the process id to pin this window to all desktops
						//Thread.Sleep(1000);
						Task.Delay(1000).Wait();
						Process oCurrent = Process.GetCurrentProcess();
						System.IntPtr iParam = oCurrent.MainWindowHandle;
						if (verbose) Console.WriteLine("MainWindowHandle: {0}", iParam.ToString());
						//VirtualDesktop11.Desktop.PinWindow(iParam);
						if (verbose) Console.WriteLine("Starting background loop");
						while (true) 
						{

							//hightlight active desktop button if change of virtual desktop was done outside this tool
							HighlightActiveDesktopButton();

							int millisecondsToWait = 100;

							Stopwatch stopwatch = Stopwatch.StartNew();
							while (true)
							{
								//some other processing to do possible
								if (stopwatch.ElapsedMilliseconds >= millisecondsToWait)
								{
									break;
								}
								Task.Delay(1).Wait();
								//Thread.Sleep(1);
							}

							//Thread.Sleep(500);
						}


					} );


					//
					// TODO: Add any constructor code after InitializeComponent call
					//
				}

				/// <span class="code-SummaryComment"><summary></span>
				/// Clean up any resources being used.
				/// <span class="code-SummaryComment"></summary></span>
				protected override void Dispose( bool disposing )
				{
					if( disposing )
					{
						if (components != null) 
						{
							components.Dispose();
						}
					}
					base.Dispose( disposing );
				}

				#region Windows Form Designer generated code
				/// <span class="code-SummaryComment"><summary></span>
				/// Required method for Designer support - do not modify
				/// the contents of this method with the code editor.
				/// <span class="code-SummaryComment"></summary></span>

				private static TableLayoutPanel panel;
				
				private void InitializeComponent()
				{
					int cols = 4;
					int rows = 1;
					int x =0 ;
					if (ConfigIni.KeyExists("cols","size")) {
						cols = Int16.Parse(ConfigIni.Read("cols","size"));
					}
					if (ConfigIni.KeyExists("rows","size")) {
						rows = Int16.Parse(ConfigIni.Read("rows","size"));
					}
					
					if (verbose) Console.WriteLine("[size]");
					if (verbose) Console.WriteLine("cols=" + cols);
					if (verbose) Console.WriteLine("rows=" + rows);

					if (ConfigIni.KeyExists("virtdeskbarHeight","size")) {
						virtdeskbarHeight = Int16.Parse(ConfigIni.Read("virtdeskbarHeight","size"));
					}
					if (verbose) Console.WriteLine("virtdeskbarHeight=" + virtdeskbarHeight);


					//Check how many buttons are configured
					int configuredButtonsCount = 0;
					for (int i = 1; i <= maxIconButtons ; i++)
					{
						var cfgButtonKey = "button" + i;
						if (ConfigIni.KeyExists("iconPath",cfgButtonKey)) configuredButtonsCount++;
					}
					int iconsColumnSize = configuredButtonsCount * virtdeskbarHeight; //(=iconWidth)
					int virtdeskMatrixWidth = 40 * cols;



					// 
					// MainForm
					// 
					this.AutoScaleBaseSize = new System.Drawing.Size(5, 15);
					this.ClientSize = new System.Drawing.Size(960, 20);
					this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
					this.Name = "virtdeskbar";
					this.Text = "AppBar";
					this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
					this.Load += new System.EventHandler(this.OnLoad);
					this.BackColor = backgroundColor;









					TableLayoutPanel mainLayout = new TableLayoutPanel();
					mainLayout.ColumnCount = 5;
					mainLayout.RowCount = 1;
					mainLayout.Dock = DockStyle.Fill;
					mainLayout.BackColor = Color.FromArgb(0, 0, 0, 0);
					mainLayout.BorderStyle = BorderStyle.None;

					mainLayout.ColumnStyles.Clear();
					//menu part on left side in with an absolute width
					//main area: Use 100% of the rest space
					//mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent,100));
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, iconsColumnSize)); //Icons
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, virtdeskMatrixWidth)); // Virtdesk matrix width
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
					mainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20)); //For close button



					mainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100));


					panel = new TableLayoutPanel();
					panel.ColumnCount = cols;
					panel.RowCount = rows;
					panel.Dock = DockStyle.Fill;
					panel.BackColor = Color.FromArgb(0, 0, 0, 0);
					panel.BorderStyle = BorderStyle.None;

					panel.Margin = new Padding(0, 0, 0, 0);


					//panel.BorderColor.FromArgb(255,64,64,64);
					//panel.BorderStyle = BorderStyle.None;

					panel.ColumnStyles.Clear();

					//desktop matrix size

					for (int i = 0; i < cols; i++)
					{
						ColumnStyle cs = new ColumnStyle(SizeType.Absolute, (float)virtdeskMatrixWidth / cols);
						panel.ColumnStyles.Add(cs);
					}

					for (int i = 0; i < rows; i++)
					{
						RowStyle rs = new RowStyle(SizeType.Percent, (float)100 / rows);
						panel.RowStyles.Add(rs);
					}
					//panel.AutoSize = true;


					for (int j = 0; j < rows; j++)
					{
						for (int i = 0; i < cols; i++)
						{
							int y = x + 1;
							Button button = new Button() {
								Text=string.Format("{0}", y.ToString()), //i.ToString())
							};
							button.Visible = true;
							button.Dock = DockStyle.Fill;
							button.TabStop = false;
							// button.FlatAppearance.BorderSize = 0;
							button.Margin = new Padding(0, 0, 0, 0);
							button.BackColor = Color.FromArgb(255, 40, 40, 40); //Color.DarkGray;
							button.ForeColor = Color.FromArgb(255, 150, 150, 150); //Textcolor

							button.FlatStyle = FlatStyle.Flat;
							button.FlatAppearance.BorderColor = Color.FromArgb(255,64,64,64); 
							button.FlatAppearance.BorderSize = 1;
							button.FlatAppearance.MouseDownBackColor = virtdeskMouseHoverBackgroundColor;
							button.FlatAppearance.MouseOverBackColor = virtdeskMouseHoverBackgroundColor;

							panel.Controls.Add(button,i,j);
							x++;
						}
					}


					int ix = 0;
					
					foreach (Control c in panel.Controls.OfType<Button>()) 
					{
						c.Click += new EventHandler(VDeskOnClick);
						c.MouseEnter += new EventHandler(VDeskOnMouseEnter);
						c.MouseLeave += new EventHandler(VDeskOnMouseLeave);
						ix++;
					}

					TableLayoutPanel menuPanel = new TableLayoutPanel();

					menuPanel.ColumnCount = 1;
					menuPanel.RowCount = 2;
					menuPanel.Dock = DockStyle.Fill;
					menuPanel.BackColor = Color.FromArgb(0,0,0,0); //Transparent
					menuPanel.BorderStyle = BorderStyle.None;
					menuPanel.Margin = new Padding(0, 0, 0, 0);


					menuPanel.RowStyles.Clear();
					menuPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20));
					menuPanel.RowStyles.Add(new RowStyle(SizeType.Percent,100));
					menuPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
					menuPanel.AutoSize = true;


					Button closeButton = new Button();
					closeButton.Text = "x";
					closeButton.Click += new System.EventHandler(CloseButtonClick);
					//moveButton.Cursor = System.Windows.Forms.Cursors.SizeAll; //SizeAll = Move window Cursor

					menuPanel.Controls.Add(closeButton);



					TableLayoutPanel spacer = new TableLayoutPanel();
					spacer.ColumnCount = 1;
					spacer.RowCount = 1;
					spacer.Dock = DockStyle.Fill;
					spacer.BackColor = Color.FromArgb(0,0,0,0); //Transparent
					spacer.BorderStyle = BorderStyle.None;
					spacer.Margin = new Padding(0, 0, 0, 0);
					spacer.AutoSize = true;
					spacer.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));



					TableLayoutPanel spacer2 = new TableLayoutPanel();
					spacer2.ColumnCount = 1;
					spacer2.RowCount = 1;
					spacer2.Dock = DockStyle.Fill;
					spacer2.BackColor =Color.FromArgb(0,0,0,0); //Transparent
					spacer2.BorderStyle = BorderStyle.None;
					spacer2.Margin = new Padding(0, 0, 0, 0);
					spacer2.AutoSize = true;
					spacer2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));

					TableLayoutPanel buttonpanel = new TableLayoutPanel();
					
					buttonpanel.RowCount = 1;
					buttonpanel.Dock = DockStyle.Fill;
					buttonpanel.BackColor = Color.FromArgb(0,0,0,0); //Transparent
					buttonpanel.BorderStyle = BorderStyle.None;
					buttonpanel.Margin = new Padding(0, 0, 0, 0);
					buttonpanel.AutoSize = true;
					buttonpanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100));

					int buttonCount = 0;
					List<Button> listOfButtons = new List<Button>();

					for (int i = 1; i <= maxIconButtons; i++)
					{

						var cfgButtonKey = "button" + i;


						if (! ConfigIni.KeyExists("iconPath",cfgButtonKey)) continue;
					
						if (verbose) Console.WriteLine("Button: " + cfgButtonKey);

						Image buttonImage = resizeImage(Image.FromFile(ConfigIni.Read("iconPath",cfgButtonKey)), virtdeskbarHeight-1,virtdeskbarHeight-1);

						Button buttonExec = new Button();
						buttonExec.Image = buttonImage;
						buttonExec.ImageAlign = ContentAlignment.MiddleCenter;
						buttonExec.AutoSize = false;
						buttonExec.Size = new Size(virtdeskbarHeight,virtdeskbarHeight);
						buttonExec.Visible = true;
						buttonExec.Dock = DockStyle.Fill;
						buttonExec.TabStop = false;
						// button.FlatAppearance.BorderSize = 0;
						buttonExec.Margin = new Padding(0, 0, 0, 0);
						buttonExec.BackColor = Color.FromArgb(0,0,0,0); //Transparent
						//buttonExec.ForeColor = Color.FromArgb(255, 150, 150, 150); //Textcolor
						buttonExec.FlatStyle = FlatStyle.Flat;
						buttonExec.FlatAppearance.BorderColor = backgroundColor;
						//buttonExec.FlatAppearance.BorderColor = Color.FromArgb(255,64,64,64); 
						buttonExec.FlatAppearance.BorderSize = 0;
						buttonExec.FlatAppearance.MouseDownBackColor = buttonMouseDownBackgroundColor;
						buttonExec.FlatAppearance.MouseOverBackColor = buttonMouseHoverBackgroundColor;
						buttonExec.Click += delegate(object sender, EventArgs e)
						{ 
							ButtonClickStartExe(sender,e,ConfigIni.Read("exePath",cfgButtonKey));
						};

						listOfButtons.Add(buttonExec);

						if (verbose) {
							Console.WriteLine("image: width=" + buttonImage.Width + " height=" + buttonImage.Height);
							Console.WriteLine("button: width=" + buttonExec.Width + " height=" + buttonExec.Height);
						}


						buttonCount++;

					}

					if (verbose) Console.WriteLine("Buttons configured: " + buttonCount);


					buttonpanel.ColumnCount = buttonCount;

					for (int i = 0; i < buttonCount; i++)
					{
						//buttonpanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100));
						buttonpanel.Controls.Add(listOfButtons[i]);
					}

					
					


					mainLayout.Controls.Add(spacer);
					mainLayout.Controls.Add(buttonpanel);
					mainLayout.Controls.Add(panel);
					mainLayout.Controls.Add(spacer2);
					mainLayout.Controls.Add(menuPanel);

					this.Controls.Add(mainLayout);
				}



				public Image resizeImage(Image image, int maxWidth, int maxHeight)
				{
					var ratioX = (double)maxWidth / image.Width;
					var ratioY = (double)maxHeight / image.Height;
					var ratio = Math.Min(ratioX, ratioY);

					var newWidth = (int)(image.Width * ratio);
					var newHeight = (int)(image.Height * ratio);

					var newImage = new Bitmap(maxWidth, maxHeight);
					using (var graphics = Graphics.FromImage(newImage))
					{

						//
						// Pen blackPen = new Pen(Color.White, 1);
						// Rectangle rect = new Rectangle(0, 0, maxWidth-1, maxHeight-1);
						// graphics.DrawRectangle(blackPen, rect);

						// Calculate x and y which center the image
						int y = (maxHeight/2) - (newHeight-6) / 2;
						int x = (maxWidth/2) - (newWidth-6) / 2;
						


						// Draw image on x and y with newWidth and newHeight
						graphics.DrawImage(image, x, y, newWidth-6, newHeight-6);
					}

					return newImage;
				}




				static void ButtonClickStartExe(object sender, EventArgs e, string exePath) 
				{
					Process.Start(exePath);
				} 

				static void CloseButtonClick(object sender, EventArgs e) 
				{
					System.Windows.Forms.Application.Exit();
				}



		static void VDeskOnMouseEnter(object sender, EventArgs e) 
		{
			Button button = sender as Button;
			button.FlatAppearance.BorderColor = virtdeskMouseHoverBorderColor;
			//button.ForeColor = Color.FromArgb(255, 250, 250, 250);
		}

		static void VDeskOnMouseLeave(object sender, EventArgs e) 
		{
			Button button = sender as Button;
			button.FlatAppearance.BorderColor = Color.FromArgb(255,64,64,64);
			//button.ForeColor = Color.FromArgb(255, 150, 150, 150);
		}


		static void VDeskOnClick(object sender, EventArgs e) 
		{
			Button button = sender as Button;
			string s = button.Text;
			//Console.WriteLine(s);
			//button.Visible = false;

			int column = panel.GetPositionFromControl(button).Column;
			int row = panel.GetPositionFromControl(button).Row;

			int iParam;
			int rc;
			
			if (int.TryParse(s, out iParam))
			{ // parameter is an integer, use as desktop number
			iParam--;
				if ((iParam >= 0) && (iParam < VirtualDesktop.Desktop.Count))
				{ // check if parameter is in range of active desktops
					if (verbose) Console.WriteLine("Switching to virtual desktop number " + iParam.ToString() + " (desktop '" + VirtualDesktop.Desktop.DesktopNameFromIndex(iParam) + "')");
					rc = iParam;
					try
					{ // activate virtual desktop iParam
						VirtualDesktop.Desktop.FromIndex(iParam).MakeVisible();
					}
					catch
					{ // error while activating
						rc = -1;
					}
				}
			} else 
			{
				MessageBox.Show("Cannot parse dekstop number");
			}

			HighlightActiveDesktopButton();

		}

		static void HighlightActiveDesktopButton() 
		{
			foreach (Control c in panel.Controls.OfType<Button>()) 
			{
				int testDesktopId;

				if (int.TryParse(c.Text, out testDesktopId)) 
				{
					testDesktopId--;

					if (VirtualDesktop.Desktop.FromIndex(testDesktopId).IsVisible == true) 
					{
						//Console.WriteLine("{0} <---",testDesktopId);
						c.BackColor = virtdeskActiveBackgroundColor;
						c.ForeColor = Color.FromArgb(255, 250, 250, 250);
					} else 
					{
						//Console.WriteLine(testDesktopId);
						c.BackColor = Color.FromArgb(255, 40, 40, 40);
						c.ForeColor = Color.FromArgb(255, 150, 150, 150);
					}
				}
			}
		}
				#endregion




				#region APPBAR

				[StructLayout(LayoutKind.Sequential)]
				struct RECT
				{
					public int left;
					public int top;
					public int right;
					public int bottom;
				}

				[StructLayout(LayoutKind.Sequential)]
				struct APPBARDATA
				{
					public int cbSize;
					public IntPtr hWnd;
					public int uCallbackMessage;
					public int uEdge;
					public RECT rc;
					public IntPtr lParam;
				}

				enum ABMsg : int
				{
					ABM_NEW=0,
					ABM_REMOVE=1,
					ABM_QUERYPOS=2,
					ABM_SETPOS=3,
					ABM_GETSTATE=4,
					ABM_GETTASKBARPOS=5,
					ABM_ACTIVATE=6,
					ABM_GETAUTOHIDEBAR=7,
					ABM_SETAUTOHIDEBAR=8,
					ABM_WINDOWPOSCHANGED=9,
					ABM_SETSTATE=10
				}

				enum ABNotify : int
				{
					ABN_STATECHANGE=0,
					ABN_POSCHANGED,
					ABN_FULLSCREENAPP,
					ABN_WINDOWARRANGE
				}

				enum ABEdge : int
				{
					ABE_LEFT=0,
					ABE_TOP,
					ABE_RIGHT,
					ABE_BOTTOM
				}

				private bool fBarRegistered = false;

				[DllImport("SHELL32", CallingConvention=CallingConvention.StdCall)]
				static extern uint SHAppBarMessage(int dwMessage, ref APPBARDATA pData);
				[DllImport("USER32")]
				static extern int GetSystemMetrics(int Index);
				[DllImport("User32.dll", ExactSpelling=true, 
					CharSet=System.Runtime.InteropServices.CharSet.Auto)]
				private static extern bool MoveWindow
					(IntPtr hWnd, int x, int y, int cx, int cy, bool repaint);
				[DllImport("User32.dll", CharSet=CharSet.Auto)]
				private static extern int RegisterWindowMessage(string msg);
				private int uCallBack;

				private void RegisterBar()
				{
					APPBARDATA abd = new APPBARDATA();
					abd.cbSize = Marshal.SizeOf(abd);
					abd.hWnd = this.Handle;
					if (!fBarRegistered)
					{
						uCallBack = RegisterWindowMessage("AppBarMessage");
						abd.uCallbackMessage = uCallBack;

						uint ret = SHAppBarMessage((int)ABMsg.ABM_NEW, ref abd);
						fBarRegistered = true;

						ABSetPos();
					}
					else
					{
						SHAppBarMessage((int)ABMsg.ABM_REMOVE, ref abd);
						fBarRegistered = false;
					}
				}

				private void ABSetPos()
				{
					APPBARDATA abd = new APPBARDATA();
					abd.cbSize = Marshal.SizeOf(abd);
					abd.hWnd = this.Handle;
					abd.uEdge = (int)ABEdge.ABE_BOTTOM;

					if (abd.uEdge == (int)ABEdge.ABE_LEFT || abd.uEdge == (int)ABEdge.ABE_RIGHT) 
					{
						abd.rc.top = 0;
						abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
						if (abd.uEdge == (int)ABEdge.ABE_LEFT) 
						{
							abd.rc.left = 0;
							abd.rc.right = Size.Width;
						}
						else 
						{
							abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
							abd.rc.left = abd.rc.right - Size.Width;
						}

					}
					else 
					{
						abd.rc.left = 0;
						abd.rc.right = SystemInformation.PrimaryMonitorSize.Width;
						if (abd.uEdge == (int)ABEdge.ABE_TOP) 
						{
							abd.rc.top = 0;
							abd.rc.bottom = Size.Height;
						}
						else 
						{
							abd.rc.bottom = SystemInformation.PrimaryMonitorSize.Height;
							abd.rc.top = abd.rc.bottom - Size.Height;
						}
					}

					// Query the system for an approved size and position. 
					SHAppBarMessage((int)ABMsg.ABM_QUERYPOS, ref abd); 

					// Adjust the rectangle, depending on the edge to which the 
					// appbar is anchored. 
					switch (abd.uEdge) 
					{ 
						case (int)ABEdge.ABE_LEFT: 
							abd.rc.right = abd.rc.left + Size.Width;
							break; 
						case (int)ABEdge.ABE_RIGHT: 
							abd.rc.left= abd.rc.right - Size.Width;
							break; 
						case (int)ABEdge.ABE_TOP: 
							abd.rc.bottom = abd.rc.top + virtdeskbarHeight;//Size.Height;
							break; 
						case (int)ABEdge.ABE_BOTTOM: 
							abd.rc.top = abd.rc.bottom - virtdeskbarHeight; //Size.Height;
							break; 
					}

					// Pass the final bounding rectangle to the system. 
					SHAppBarMessage((int)ABMsg.ABM_SETPOS, ref abd); 

					// Move and size the appbar so that it conforms to the 
					// bounding rectangle passed to the system. 
					MoveWindow(abd.hWnd, abd.rc.left, abd.rc.top, 
						abd.rc.right - abd.rc.left, abd.rc.bottom - abd.rc.top, true); 
				}

				protected override void WndProc(ref System.Windows.Forms.Message m)
				{
					if (m.Msg == uCallBack)
					{
						switch(m.WParam.ToInt32())
						{
							case (int)ABNotify.ABN_POSCHANGED:
								ABSetPos();
								break;
					}
					}

					base.WndProc(ref m);
				}

				protected override System.Windows.Forms.CreateParams CreateParams
				{
					get
					{
						CreateParams cp = base.CreateParams;
						cp.Style &= (~0x00C00000); // WS_CAPTION
						cp.Style &= (~0x00800000); // WS_BORDER
						cp.ExStyle = 0x00000080 | 0x00000008; // WS_EX_TOOLWINDOW | WS_EX_TOPMOST
						return cp;
					}
				}

				#endregion


				public class Options
				{
					[Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
					public bool Verbose { get; set; }

					[Option('c', "config", Required = false, HelpText = "Config file")]
					public string ConfigFile { get; set; }

				}


				/// <span class="code-SummaryComment"><summary></span>
				/// The main entry point for the application.
				/// <span class="code-SummaryComment"></summary></span>

				[DllImport( "kernel32.dll" )]
				static extern bool AttachConsole( int dwProcessId );
				private const int ATTACH_PARENT_PROCESS = -1;

				[STAThread]
				static void Main(string[] args) 
				{
//					Console.WriteLine("Hello");

					bool verb = false;
					string configfile = "virtdeskbar.ini";

					var parser = new CommandLine.Parser(with => with.HelpWriter = null);

					var parserResult = parser.ParseArguments<Options>(args);
					parserResult.WithParsed<Options>(o =>
						{
							if (o.Verbose)
							{
								verb = true;
							}

							if (o.ConfigFile != "")
							{
								configfile = o.ConfigFile;
							}


						}).WithNotParsed(errs => DisplayHelp(parserResult, errs));



					if (verb == true) {
						AttachConsole( ATTACH_PARENT_PROCESS );
						Console.WriteLine("");
						Console.WriteLine("Console attached");
					}

					if (verb) {
						Console.WriteLine("Using config file: " + configfile);
					}
					


					Application.Run(new MainForm(verb,configfile));
				}

				static void DisplayHelp<T>(ParserResult<T> result, IEnumerable<Error> errs)
				{

					AttachConsole( ATTACH_PARENT_PROCESS );
					Console.WriteLine("");

					HelpText helpText = null;
					if (errs.IsVersion())  //check if error is version request
						helpText = HelpText.AutoBuild(result);
					else
					{
						helpText = HelpText.AutoBuild(result, h =>
						{
							//configure help
							h.AdditionalNewLineAfterOption = false;
							h.Heading = "virtdeskbar"; //change header
							h.Copyright = "Copyright (c) 2022 Korbinian Pauli"; //change copyright text
							return HelpText.DefaultParsingErrorsHandler(result, h);
						}, e => e);
					}
					Console.WriteLine(helpText);
					System.Environment.Exit(0);
				}


				private void OnLoad(object sender, System.EventArgs e)
				{
						RegisterBar();



				}

				private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
				{
						RegisterBar();
				}
		}
}
