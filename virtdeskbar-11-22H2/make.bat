@echo off
:: Based on:
:: Markus Scholtes, 2021
:: Compile VirtualDesktop in .Net 4.x environment
setlocal
echo ------------------
::C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe "%~dp0VirtualDesktop11.cs" /win32icon:"%~dp0MScholtes.ico"
C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe -reference:"packages\CommandLineParser.2.8.0\lib\net40\CommandLine.dll" -target:winexe -out:virtdeskbar.exe "%~dp0*.cs" 
echo ------------------
echo DONE

:: was batch started in Windows Explorer? Yes, then pause
@REM echo "%CMDCMDLINE%" | find /i "/c" > nul
@REM if %ERRORLEVEL%==0 pause